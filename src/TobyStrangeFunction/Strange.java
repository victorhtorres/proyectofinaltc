
package TobyStrangeFunction;

import java.util.Arrays;

public class Strange {
    
 /* Atributos de la clase.*/  
    
    //Variable que recibe la cadena de entrada capturada en dataaCadena[j]
    private static char[] cadena ;
    
    //Variable que recibe el numero de Movimientos a realizar en la cadena (número de casos)
    //Se utiliza double por si en la entrada se utiliza un numero mayor a 10^18
    private double T;
    
    /* Método que captura los dos Arreglos de cadenas de Entrada.*/
      public Strange(String []dataaCadena, String []dataEntera){
        
        //Recorrido del Arreglo []dataaCadena para capturar cada subcadena
        for(int j=0; j<dataaCadena.length; j++){
            
            /* Procedemos a separar en subcadenas los datos del Arreglo dataaCadena[].*/
            //Se guarda el dato de la posicion dataaCadena[j] en un arreglo tipo char cadena
            //Se utiliza .toCharArray() para guardar el dato como un rreglo (mejor que split en tiempo de ejecucion)
            this.cadena = dataaCadena[j].toCharArray() ;
            
            //Se utiliza una variable auxiliar que recibira la nueva cadena despues de iterar sus elementos
            char[] aux = new char[cadena.length];
            
            //Se captura el numero de movimientos para realizar la validacion 
            this.T = (Double.parseDouble(dataEntera[j]));
                
                //Validacion de la cadena
                if(validarCadena(dataaCadena[j])){ 
                    
                    //Validacion del número de movimientos T
                    if(validarEntero(T)){
                        
                        //Se crea variable movimientos que recibe la cantidad de iteraciones de cada cadena
                        //Se trabaja con variable long dado el tamaño de la entrada
                        long movimientos = Long.parseLong(dataEntera[j]);  
                        
                        //Si el residuo de la divicion del numero de movimientos
                        //Entre el largo de la cadena es cero la cadena nueva quedara igual a la de entrada
                        long y = movimientos % cadena.length;
                        
                        
                        if(y == 0){
                            //Se imprime resultado si la cadena queda igual
                            System.out.println("Cadena "+(j+1)+" Ingresada: "+Arrays.toString(cadena)+"\n"+
                            "Cadena Nueva    : "+Arrays.toString(cadena)+"\n"+
                            "No. Movimientos : "+ movimientos +"\n");
                        }else{
                            //Si el residuo es mayor la cadena debera cambiar las posiciones de sus datos
                            //Se iteran las posiciones de la cadena segun el valor de la variable movimientos
                            for(int i=0; i < cadena.length; i++){
                                
                                //Dado que movimientos es un dato Long se convierte y almacena despues de operar en la variable posicion
                                Integer posicion = (int)((i+movimientos)%cadena.length);
                                aux[posicion] = cadena[i]; 
                            }
                            
                            //Se imprime cadena reusltante despues de iterar
                            System.out.println("Cadena "+(j+1)+" Ingresada: "+Arrays.toString(cadena)+"\n"+
                            "Cadena Nueva    : "+Arrays.toString(aux)+"\n"+
                            "No. Movimientos : "+ movimientos +"\n");
                        } 
                    }else{
                          //Si no se superla la validacion del número de movimientos imprime error
                          System.out.println("Cadena "+(j+1)+" Ingresada: "+Arrays.toString(cadena)+"\n"+
                          "Error.Numero de movimientos Invalido. Menor que 0 Superior a 10^18"+"\n"+
                          "No. Movimientos : "+ T +"\n");}
                }else{
                    //Si no se superla la validacion de la cadena imprime error
                    System.out.println("Cadena "+(j+1)+" Ingresada: "+Arrays.toString(cadena)+"\n"+
                    "Error. La cadena contiene caracteres no validos o es superior a 100 caracteres"+"\n"+
                     "Solo se permiten letras latinas minúsculas"+"\n"+       
                    "No. Movimientos : "+ T +"\n");}

        }
    }       

    /* Método que valida la subcadena (solo minusculas latinas).*/
    public static boolean validarCadena(String subCadena){ 
        
        //Se recorre la cadena
        for(int i = 0; i < subCadena.length(); i++){ 
            
            //Haciendo uso del código ascii aceptamos solo minúsculas
            //El rango de minúsculas valido es de 97 a 122
            //Se utiliza .charAt(i) para evaluar cada letra y retornar su codigo ascci
            //la cadena deber ser de longitud < = 100
            if((!(subCadena.charAt(i) > 96 && subCadena.charAt(i) < 123)||cadena.length > 100)) 
                return false; 
            }
            return true;
        }
    
    /* Método que valida el numero de casos de prueba >= 0 y <= 10^8 ).*/
    public static boolean validarEntero(double T){ 
        
            if (T <= Math.pow(10, 18) && (T > 0)){
                 return true; 
            } 
            return false;
    }
}
