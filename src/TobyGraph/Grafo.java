package TobyGraph;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Clase que implementa un grafo no dirigido por medio de una lista de adyacencia.
 * Ideal para casos de grafos dispersos.
 */
public class Grafo<T> {

    private HashMap<Integer, LinkedList<Integer>> datos; // Mapeo de los vértices con sus adyacencias.
    
    /**
     * El constructor que crea la lista de adyacencia para representar un grafo no dirigido
     *
     * @param nVertices número de vertices.
     * @param coor arreglo de coordenadas del punto A al punto B.
     */
    public Grafo(int nVertices, Coordenada[] coor) {
        datos = new HashMap<>();
        // restricciones del problema
        if ((1 <= nVertices && nVertices <= (Math.pow(10, 4)) && 0 <= coor.length && coor.length <= (nVertices - 1))) {
            // Crear los vertices del grafo en el HashMap.
            for (int i = 1; i <= nVertices; i++) {
                datos.put(i, new LinkedList<Integer>());
            }
            // Crear las adyacencias de los vertices.
            adyacencia(coor);
        } else {
            System.out.println("Error: No se está cumpliendo con la reestricción del problema:\n"
                    + "n(1 <= n <= 10^4) y m(0 <= m <= n-1)");
        }

    }

    /**
     * Método que crea las adyacencias del vertice A al vertice B.
     * @param coor
     */
    private void adyacencia(Coordenada[] coor) {

        int a;
        int b;
        // listas para guardar las adyacencias de los vertices.
        LinkedList<Integer> lista = new LinkedList<>();
        LinkedList<Integer> lista2 = new LinkedList<>();

        for (Coordenada iter : coor) {
            a = iter.getX();
            b = iter.getY();
            
            // restricción del problema
            if (1 <= a && a <= b && b <= datos.size()) {
                // obtener las adyacencias del vertice X y agrega el valor Y como nueva adyacencia.
                lista = datos.get(a);
                lista.add(b);
                datos.put(a, lista);

                // Como es un grafo no dirigido, entonces el vertice Y también debe ser adyacente al vertice X.
                lista2 = datos.get(b);
                lista2.add(a);
                datos.put(b, lista2);
            }else{
                System.out.println("Error en la coordenada a,b(" + a + ", " + b + "). No se está cumpliendo con la reestricción del problema:\n" +
                                    "a,b(1 <= a <= b <= n)");
            }

        }

    }

    /**
     * Método que muestra todas las uniones de conjuntos en el grafo
     */
    public void conjuntos() {
        nuevosConjuntos(conjuntosActuales());

    }
    
    
    /**
     * Método que crear los conjuntos que existen actualmente en el grafo
     * @return listaConjuntos
     */
    private LinkedList<String> conjuntosActuales() {
        LinkedList<Integer> ady; // lista temporal de vertices adyacentes.
        LinkedList<String> listaConjuntos = new LinkedList<>();
        boolean[] visitados = new boolean[datos.size()];
        int vActual; // vertice actual.
        String conjunto = ""; // concatena los vertices que forman un conjunto.

        // recorrer todos los vertices y forma conjuntos con los vertices que sean adyacentes al valor i+1.
        for (int i = 0; i < datos.size(); i++) {
            if (!visitados[i]) {
                visitados[i] = true;
                conjunto += i + 1;
                ady = datos.get(i + 1);
                while (ady.size() > 0) {
                    vActual = ady.remove(0);
                    if (!visitados[vActual - 1]) {
                        visitados[vActual - 1] = true;
                        conjunto += " " + vActual;
                        ady.addAll(datos.get(vActual));
                    }
                }
                listaConjuntos.add(conjunto);
                conjunto = ""; // resetea el valor conjunto para concatenar nuevos conjuntos.
            }
        }

        return listaConjuntos;
    }
    
    
    /**
     * Método que crea nuevos conjuntos a partir de los ya existentes en el
     * grafo
     *
     * @param conjuntosActuales es la lista de conjuntos actuales
     */
    private void nuevosConjuntos(LinkedList conjuntosActuales) {
        int contador = 0;
        String conjunto = "";

        // formar nuevos conjuntos a partir de los ya existentes.
        for (int i = 0; i < conjuntosActuales.size(); i++) {
            for (int j = i + 1; j < conjuntosActuales.size(); j++) {
                contador++;
                conjunto += "Salida" + contador + ": " + conjuntosActuales.get(i) + " "
                        + conjuntosActuales.get(j) + "\n";
            }
        }

        System.out.println("Numero de nuevos conjuntos: " + contador);
        System.out.println(conjunto);
    }
    
    
    /**
     * Muestra el grafo representado en una lista adyacente
     *
     * @return string
     */
    @Override
    public String toString() {

        StringBuilder string = new StringBuilder();

        for (Integer key : datos.keySet()) {
            string.append(key).append(":").append(datos.get(key)).append("\n");
        }
        return string.toString();
    }

}
